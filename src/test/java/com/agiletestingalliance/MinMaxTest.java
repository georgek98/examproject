package com.agiletestingalliance;


import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {
    
    
    @Test
    public void testcompareA() throws Exception {

        int myNumv= new MinMax().comparef(2,1);
        assertEquals("Error in mul", 2, myNumv);
        
    }
    @Test
    public void testcompareB() throws Exception {

        int myNumv= new MinMax().comparef(1,2);
        assertEquals("Error in mul", 2, myNumv);
        
    }

    @Test
    public void testbar() throws Exception {

        String testingbout= new MinMax().bar("test");
        assertTrue("Error in sentence", testingbout.contains("test"));
        
    }
}

