package com.agiletestingalliance;


import org.junit.Test;
import static org.junit.Assert.*;

public class AboutCPDOFTest {
    @Test
    public void testAbout() throws Exception {

        String testingbout= new AboutCPDOF().desc();
        assertTrue("Error in sentence", testingbout.contains("solidify"));
        
    }
}

